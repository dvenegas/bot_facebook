 const horariosDomingo = (recipientId, URL) => {
    return {
    recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: URL + "/assets/horariosDomingo.png"
                }
            }
        }
    }
}

const horariosSabado = (recipientId, URL) => {
    return {
    recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: URL + "/assets/horariosSabado.png"
                }
            }
        }
    }
}

const ticketsButton = (recipientId, URL) => {
    return {
      recipient: {
        id: recipientId
      },
      message: {
        attachment: {
          type: "template",
          payload: {
            template_type: "generic",
            elements: [{
              title: "Corona Capital",
              subtitle: "Compra boletos para Corona Capital en concierto y encuentra el calendario de fechas de concierto en el sitio oficial de venta Ticketmaster MX.",
              item_url: "http://www.ticketmaster.com.mx/Corona-Capital-boletos/artist/1608797",
              image_url: "http://s1.ticketm.net/dam/a/1f1/31a84b04-e9af-4f78-9ccc-08955d3c81f1_145521_EVENT_DETAIL_PAGE_16_9.jpg",
            }]
          }
        }
      }
    };
}

const lineUp = (recipientId, URL) => {
    return {
      recipient: {
        id: recipientId
      },
      message: {
        attachment: {
          type: "video",
          payload: {
            url: URL + "/assets/capital.mp4"
          }
        }
      }
    };
}


module.exports = {
      HorarioD: horariosDomingo,
      HorarioS: horariosSabado,
      Tickets: ticketsButton,
      LineUp: lineUp
}
