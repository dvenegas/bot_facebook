curl -X POST -H "Content-Type: application/json" -d '{
  "persistent_menu":[
    {
      "locale":"default",
      "composer_input_disabled":true,
      "call_to_actions":[
        {
          "title":"Horarios",
          "type":"nested",
          "call_to_actions":[
            {
              "title":"Sábado por hora",
              "type":"postback",
              "payload":"BTN_SATURDAY_TIMETABLE_PAYLOAD"
            },
            {
                "title":"Sábado por escenario",
                "type":"postback",
                "payload":"BTN_SATURDAY_STAGE_PAYLOAD"
            },
            {
                "title":"Domingo por hora",
                "type":"postback",
                "payload":"BTN_SUNDAY_TIMETABLE_PAYLOAD"
            },
            {
                "title":"Domingo por escenario",
                "type":"postback",
                "payload":"BTN_SUNDAY_STAGE_PAYLOAD"
            }]
        },
        {
            "title":"Line-Up",
            "type":"postback",
            "payload":"BTN_LITEUP_PAYLOAD"
        },
        {
            "title":"Boletos",
            "type":"postback",
            "payload":"BTN_TIKECTS_PAYLOAD"
        }
      ]
    },
    {
      "locale":"zh_CN",
      "composer_input_disabled":false
    }
  ]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAXXtt40t4sBAFCgwAoR8J2DZA5iQKLDrjBDMzYrmk0pokV9AaShxnBZCl0O2s4JYkqlHAuM0KFNjeq7PFvIQJc59QFTlRgbR3OIN0V3wrGVdsXOFUZAK8GgywWuhMZA5cxj7FJWkhiFbjG46gZCZChC6FIR0VOMCBYm0vc58yvQZDZD"

curl -X GET "https://graph.facebook.com/v2.6/me/messenger_profile?fields=persistent_menu&access_token=EAAXXtt40t4sBAFCgwAoR8J2DZA5iQKLDrjBDMzYrmk0pokV9AaShxnBZCl0O2s4JYkqlHAuM0KFNjeq7PFvIQJc59QFTlRgbR3OIN0V3wrGVdsXOFUZAK8GgywWuhMZA5cxj7FJWkhiFbjG46gZCZChC6FIR0VOMCBYm0vc58yvQZDZD"


curl -X POST -H "Content-Type: application/json" -d '{
  "persistent_menu":[
    {
      "locale":"default",
      "composer_input_disabled":true,
      "call_to_actions":[
        {
          "title":"My Account",
          "type":"nested",
          "call_to_actions":[
            {
              "title":"Pay Bill",
              "type":"postback",
              "payload":"PAYBILL_PAYLOAD"
            },
            {
              "title":"History",
              "type":"postback",
              "payload":"HISTORY_PAYLOAD"
            },
            {
              "title":"Contact Info",
              "type":"postback",
              "payload":"CONTACT_INFO_PAYLOAD"
            }
          ]
        },
        {
            "title":"Line-Up",
            "type":"postback",
            "payload":"BTN_LITEUP_PAYLOAD"
        },
        {
            "title":"Boletos",
            "type":"postback",
            "payload":"BTN_TIKECTS_PAYLOAD"
        },
      ]
    },
    {
      "locale":"zh_CN",
      "composer_input_disabled":false
    }
  ]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAXXtt40t4sBAFCgwAoR8J2DZA5iQKLDrjBDMzYrmk0pokV9AaShxnBZCl0O2s4JYkqlHAuM0KFNjeq7PFvIQJc59QFTlRgbR3OIN0V3wrGVdsXOFUZAK8GgywWuhMZA5cxj7FJWkhiFbjG46gZCZChC6FIR0VOMCBYm0vc58yvQZDZD"
